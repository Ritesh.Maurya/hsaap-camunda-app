package nz.govt.health.hsaap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "nz.govt.health.hsaap")
public class CamundaApplication {

    public static void main(String... args) {
        SpringApplication.run(CamundaApplication.class, args);
    }

}